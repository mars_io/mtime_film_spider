# -*- coding: utf-8 -*-
import scrapy
from time_film.items import TimeFilmItem

class MtimeSpider(scrapy.Spider):
    name = 'mtime'
    allowed_domains = ['www.mtime.com']
    start_urls = ['http://www.mtime.com/top/movie/top100/']

    def parse(self, response):
        
        for movie_item in response.css("#asyncRatingRegion li"):
            item = TimeFilmItem()
            item['movie_title'] = movie_item.css(".mov_con h2 a::text").extract()
            item['movie_director'] = movie_item.css(".mov_con p:first-of-type a::text").extract()
            item['movie_star']  = movie_item.css(".mov_con p:nth-of-type(2) a::text").extract()
            item['movie_type']  = movie_item.css(".mov_con p:nth-of-type(3) span a::text").extract()
            item['movie_intro']  = movie_item.css(".mov_con p:nth-of-type(4)::text").extract()
            point1 = movie_item.css(".mov_point span:nth-of-type(1)::text").extract()
            point2 = movie_item.css(".mov_point span:nth-of-type(2)::text").extract()
            item['movie_point'] = point1[0] + point2[0] if point1[0] and point2[0] else "Unknow"
            
            for key, val in item.items():
                if isinstance(val, list):
                    item[key] = ",".join(val)

            yield item

        links = [self.start_urls[0] + "index-{}.html".format(i) for i in range(2, 11)]
        current_page = int(response.css(".top_list ul li div em::text").extract_first())
        
        if current_page < 10:
            yield  scrapy.Request(links[current_page-1], callback = self.parse)
