# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html


class TimeFilmPipeline(object):

    md_file_handle = None
    md_file_text = ""

    def __init__(self):
        self.md_file_handle = open("./mtime.result.md", "w+")
        self.md_file_text = "| FilmTitle | FilmDirector | FilmStar | FilmType | FilmIntro | filmPoint | \n"
        self.md_file_text = self.md_file_text + "|----------|:------:|:------:|:------:|:------:|------:|\n"
        self.md_file_handle.writelines(self.md_file_text)

    '''
        Output as markdown table, and show top 100 films
    '''
    def process_item(self, item, spider):
        self.md_file_handle.writelines("| {} | {} | {} | {} | {} | {} |\n".format(item['movie_title'], 
                                                                        item['movie_director'], 
                                                                        item['movie_star'] ,
                                                                        item['movie_type'], 
                                                                        item['movie_intro'], 
                                                                        item['movie_point']))

    def close_spider(self, spider):
        self.md_file_handle.close()