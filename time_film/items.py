# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class TimeFilmItem(scrapy.Item):
    movie_title = scrapy.Field()
    movie_director = scrapy.Field()
    movie_type = scrapy.Field()
    movie_star = scrapy.Field()
    movie_intro = scrapy.Field()
    movie_point = scrapy.Field()